package pl.com.adapat.safecloud.comm;

import java.io.Serializable;

import pl.com.adapat.safecloud.crypto.RfcHmac;

/**
 * Created by patryk on 21.05.15.
 */


/**
 * KLASA DO USUNIĘCIA
 */
public class FileMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    //TODO: wymyślić formę transferu pliku
    //TODO: póki co rozważam byte[]

    //TODO: wymyślić jak liczyć z tego wszystkiego hash

    /*
    Struktura wiadomości przenoszących pliki
    Wiadomości będą sparsowane do JSONa
    --------------
    | timestamp  | czas utworzenia wiadomości
    --------------
    | username   | użytkownik który utworzył wiadomość
    -------------- > nie wiem co tu dać w przypadku serwera
    | command    | typ komendy (wypisane niżej)
    --------------
    | file       | plik do przesłania
    --------------
    | hmac       | HMAC wiadomości wyliczony na podstawie:
    -------------- > konkatenacja wszystkich powyższych pól wiadomości + zhekoswany plik (?)
                    można też wszystkie pola skonwertować na byte[]
                   > klucz prywatny (nie hasło użytkownika)
     */
    public String timestamp;
    public String username;
    public String command;
    public byte[] file;
    public String hmac;

    /*
    Lista typów wiadomości
    TODO: dodać negatywne odpowiedzi

    nazwa              kierunek        argument                        opis


    FILE_TRANSFER_PULL    S->K          PLIK

    FILE_TRANSFER_PUSH    K->S          PLIK

            -

     */
    public static final String TYPE_REGISTER_REQ = "REGISTER_REQ";
    public static final String TYPE_REGISTER_ACK = "REGISTER_ACK";

    public static final String TYPE_GET_LIST = "GET_LIST";
    public static final String TYPE_LIST = "LIST";

    public static final String TYPE_PULL_REQ = "PULL_REQ";
    public static final String TYPE_FILE_TRANSFER_PULL = "FILE_TRANSFER_PULL";

    public static final String TYPE_PUSH_REQ = "PUSH_REQ";
    public static final String TYPE_PUSH_ACK = "PUSH_ACK";
    public static final String TYPE_FILE_TRANSFER_PUSH = "FILE_TRANSFER_PUSH";

    public static final String TYPE_DEL_REQ = "DEL_REQ";
    public static final String TYPE_DEL_ACK = "DEL_ACK";


    public FileMessage(String type, byte[] file){

        long time = System.currentTimeMillis();
        this.timestamp = String.valueOf(time);

        this.username = AuthData.username;

        this.command = type;

        this.file = file;

        if (command.equals(TYPE_REGISTER_REQ) || command.equals(TYPE_REGISTER_ACK)){

            //to się w ogóle nie powinno stać
            this.hmac = "0000";

        } else {

            //na razie zapisujemy bajty pliku w hexach
            String message_text = timestamp+username+command+RfcHmac.hexify(file);
            this.hmac = RfcHmac.hmac(AuthData.privateKey,message_text);

        }


    }

    // sprawdzamy czy hmac dostarczony z wiadomością zgada się z tym policzonym
    public boolean verify(String key){


        String message_text = timestamp+username+command+RfcHmac.hexify(file);
        String calculated_hmac = RfcHmac.hmac(key,message_text);

        long timeout=AuthData.timeout*1000;
        if (calculated_hmac.equals(this.hmac) && (System.currentTimeMillis() - Long.valueOf(this.timestamp).longValue() < timeout)){
            return true;
        }

        return false;
    }


}

