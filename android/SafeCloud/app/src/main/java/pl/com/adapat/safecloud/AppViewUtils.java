package pl.com.adapat.safecloud;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.Arrays;
import java.util.List;

import pl.com.adapat.safecloud.comm.Message;

/**
 * Created by patryk on 13.06.15.
 */

/**
 * Klasa z metodami automatyzujacymi operacje GUI
 */
public class AppViewUtils {


    /**
     * rozdzielenie listy plikow z wiadomosci na potrzeby adaptera do ListView
     *
     * @param m wiadomosc (typu LIST)
     * @return lista nazw plikow
     */
    public static List<String> getFilesListFromMessage(Message m){

        return Arrays.asList(m.getArgument().split(","));
    }


    /**
     * Wyswietlenie okna dialogowego z informacja
     *
     * @param context kontekst aktywnosci na ktorej ma zostac wyswietlony dialog
     * @param title tytul
     * @param message wiadomosc
     */
    public static void displayDialog(Context context, String title, String message){

        AlertDialog.Builder builderInfo = new AlertDialog.Builder(context);
        builderInfo.setMessage(message);
        builderInfo.setTitle(title);
        builderInfo.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int which) {
                        dialog.dismiss();
                    }
                });
        builderInfo.show();

    }



}
