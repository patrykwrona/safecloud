package pl.com.adapat.safecloud;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.commons.io.FileUtils;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.com.adapat.safecloud.comm.AuthData;
import pl.com.adapat.safecloud.comm.Message;


/**
 * Klasa zawierająca liste plikow uzytkownika<br>
 * Realizuje wiekszosc zadan aplikacji: pobieranie, wysylanie, usuwanie plikow, pobieranie listy plikow z serwera
 */
public class FilesActivity extends ListActivity {

    int FILE_CHOOSER = 1;

    /** lista plikow  */
    List<String> listNames;

    /** adapter listy plikow  */
    ArrayAdapter<String> myAdapter;

    /** dialog postepu operacji */
    static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);

        listNames = new ArrayList<String>();

        progressDialog = ProgressDialog.show(FilesActivity.this,"","Pobieranie listy plikow...");
        new GetFilesListTask().execute();

////        //example files for testing
//        listNames.add("file1.txt");
//        listNames.add("file2.exe");
//        listNames.add("file3.jpg");

        // initiate the listadapter
        myAdapter = new ArrayAdapter <String>(this,
                R.layout.files_list_record, R.id.id_fileName, listNames);

        // assign the list adapter
        setListAdapter(myAdapter);

    }

    // when an item of the list is clicked
    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);

        final String selectedItem = (String) getListView().getItemAtPosition(position);

        AlertDialog.Builder fileActionBuilder = new AlertDialog.Builder(this);
        fileActionBuilder.setTitle("Wybierz akcje z: "+selectedItem);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.select_dialog_item);
        arrayAdapter.add("Pobierz");
        arrayAdapter.add("Usuń");
        fileActionBuilder.setNegativeButton("Anuluj",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        fileActionBuilder.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(FilesActivity.this);
                        builderInner.setMessage(strName+": "+selectedItem);
                        builderInner.setTitle("Potwierdź wykonanie");

                        final int innerWhich = which;

                        builderInner.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        if (innerWhich==0){
                                            progressDialog = ProgressDialog.show(FilesActivity.this,"","Pobieranie pliku...");
                                            new DownloadFileTask().execute(selectedItem);
                                        }
                                        if (innerWhich==1){
                                            progressDialog = ProgressDialog.show(FilesActivity.this,"","Usuwanie pliku...");
                                            new DeleteFileTask().execute(selectedItem);
                                        }


                                        dialog.dismiss();
                                    }
                                });
                        builderInner.show();
                    }
                });
        fileActionBuilder.show();


    }


    /**
     * Obsługa danych (sciezki pliku) zwroconych z dialogu wyboru pliku
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == FILE_CHOOSER) && (resultCode == RESULT_OK)) {

            progressDialog = ProgressDialog.show(FilesActivity.this,"","Obliczanie HMAC wiadomosci z plikiem...");

            String fileSelected = data.getStringExtra(com.orleonsoft.android.simplefilechooser.Constants.KEY_FILE_SELECTED);
            Log.d("FILE SELECTED",fileSelected);

            // wyciagniecie ze sciezki samej nazwy pliku
            List<String> pathSegments = Arrays.asList(fileSelected.split("/"));
            String filename = pathSegments.get(pathSegments.size()-1);

            // procedura wyslania pliku
            // parametry - sama nazwa pliku, pelna sciezka
            new UploadFileTransferTask().execute(filename, fileSelected);



        //    String fileToUpload = fileSelected;
        //    MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        //    parts.add("file", new FileSystemResource(fileToUpload));
         //   RestTemplate restTemplate = new RestTemplate();
         //   String response = restTemplate.postForObject(AuthData.serverAddress+"/safecloud", parts, String.class, authToken, folderId);
         //   Log.d("UPLOAD", response);
        }
    }


    /**
     * Pobranie listy plikow z serwera
     */
    private class GetFilesListTask extends AsyncTask<Void, Void, Message> {
        @Override
        protected Message doInBackground(Void... params) {
            try {

                final String url = "http://" + AuthData.serverAddress +"/show";

                Message getlist_message = new Message(Message.TYPE_GET_LIST,"");

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());


                Message response = restTemplate.postForObject(url,getlist_message, Message.class);
                return response;

            } catch (Exception e) {
                Log.e("FilesActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Message response) {

            if (response!=null){

            // sprawdzamy czy odpowiedź jest dobrego typu oraz czy HMAC sie zgadza
            if (response.getCommand().equals(Message.TYPE_LIST) && response.verify(AuthData.privateKey)) {
                List<String> retrieved = AppViewUtils.getFilesListFromMessage(response);
                if (retrieved.size()>0){
                    myAdapter.addAll(retrieved);
                } else {
                    myAdapter.add("Nie masz plikow");
                }


            } else if (response.getCommand().equals(Message.TYPE_ERROR) && response.verify(AuthData.privateKey)) {
                AppViewUtils.displayDialog(FilesActivity.this,"Błąd",response.getArgument());
              //  finish();
            } else {
                AppViewUtils.displayDialog(FilesActivity.this, "Błąd", "Niespodziewana badź nieautoryzowana odpowiedź serwera");
              //  finish();
            }

            } else {
                AppViewUtils.displayDialog(FilesActivity.this, "Błąd", "Brak odpowiedzi");
              //  finish();
            }

            progressDialog.dismiss();

        }
    }


    /**
     * Usuniecie pliku na serwerze
     */
    private class DeleteFileTask extends AsyncTask<String, Void, Message> {

        String filenameDelete;

        @Override
        protected Message doInBackground(String... params) {
            try {

                Log.d("DELETE TASK", "params[0]:" + params[0]);

                filenameDelete = params[0];

                final String url = "http://" + AuthData.serverAddress + "/delete";
                //  final String url = "http://192.168.1.15:8080";


                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                Message del_req_message = new Message(Message.TYPE_DEL_REQ, params[0]);

                Message response = restTemplate.postForObject(url, del_req_message, Message.class);
                return response;

            } catch (Exception e) {
                Log.e("Delete task", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Message response) {

            if (response != null) {
                if (response.getCommand().equals(Message.TYPE_DEL_ACK) && response.verify(AuthData.privateKey)) {
                    AppViewUtils.displayDialog(FilesActivity.this, "OK", "Usunieto pomyslnie plik"+filenameDelete);
                    myAdapter.remove(filenameDelete);
                } else if (response.getCommand().equals(Message.TYPE_ERROR) && response.verify(AuthData.privateKey)) {
                    AppViewUtils.displayDialog(FilesActivity.this, "Blad", response.getArgument());
                    } else {
                    AppViewUtils.displayDialog(FilesActivity.this, "Blad", "Niespodziewana badź nieautoryzowana odpowiedź serwera");
                    }

            } else {
                AppViewUtils.displayDialog(FilesActivity.this, "Blad", "Brak odpowiedzi");

            }

            progressDialog.dismiss();
        }
    }


    /**
     * Wyslanie pliku na serwer
     */
    private class UploadFileTransferTask extends AsyncTask<String, Void, Boolean> {

        String filenameUpload;
        String error, errStackTrace;
        String resp;

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                final String url = "http://" + AuthData.serverAddress + "/upload";
                File file = new File(params[1]);

                filenameUpload = params[0];

                try {

                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                    Message push_file_message = new Message(Message.TYPE_FILE_TRANSFER_PUSH,
                            Base64.encodeToString(FileUtils.readFileToByteArray(file), Base64.DEFAULT));
                    push_file_message.setFilename(params[0]);
                    Log.d("FILENAME SET",params[0]);
                    Log.d("",push_file_message.toString());

                    updateProgressMessage("Wysylanie pliku...");

                    restTemplate.postForObject(url, push_file_message, Message.class);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return false;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                } catch (RestClientException e){

                    e.printStackTrace();
                    return false;

                }


            } catch (Exception e){
                Log.e("Upload Task",e.getLocalizedMessage(),e);

                return false;
            }


            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result){

                if(myAdapter.getPosition(filenameUpload)>0){
                    progressDialog.dismiss();
                    AppViewUtils.displayDialog(FilesActivity.this,"OK","Wyslano plik:"+filenameUpload);
                } else {
                    myAdapter.add(filenameUpload);
                    progressDialog.dismiss();
                    AppViewUtils.displayDialog(FilesActivity.this,"OK","Wyslano plik:"+filenameUpload);
                }

            } else {
                progressDialog.dismiss();
                AppViewUtils.displayDialog(FilesActivity.this,"Blad","Nie wyslano pliku");
            }


        }
    }

    /*
    private class UploadFileTask extends AsyncTask<String, Void, Message> {

        String filepathUpload;
        String filenameUpload;

        @Override
        protected Message doInBackground(String... params) {
            try {
                final String url = "http://" + AuthData.serverAddress +"/MkoiCloudComputing/upload";

                filepathUpload = params[1];
                filenameUpload = params[0];

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                Message push_req_message = new Message(Message.TYPE_PUSH_REQ,params[0]);

                Message response = restTemplate.postForObject(url,push_req_message, Message.class);
                return response;

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Message response) {

            if (response != null) {

                if (response.getCommand().equals(Message.TYPE_PUSH_ACK) && response.verify(AuthData.privateKey)) {

                    //wysylamy wiadomosc z plikiem

                    new UploadFileTransferTask().execute(filenameUpload,filepathUpload);
                    progressDialog.setMessage("Obliczanie HMAC wiadomosci z plikiem...");
;

                } else if (response.getCommand().equals(Message.TYPE_ERROR) && response.verify(AuthData.privateKey)) {
                    AppViewUtils.displayDialog(FilesActivity.this,"Blad",response.getArgument());
                } else {
                    AppViewUtils.displayDialog(FilesActivity.this,"Blad","Niespodziewana badź nieautoryzowana odpowiedź serwera");
                }
            } else {

                AppViewUtils.displayDialog(FilesActivity.this, "Blad", "Brak odpowiedzi, DEBUG-i tak wysylam FILE_TRANSFER_PUSH z plikiem");

                // debug:
                // FILE PUSH TRANSFER

                new UploadFileTransferTask().execute(filenameUpload,filepathUpload);
                progressDialog.setMessage("Obliczanie HMAC wiadomosci z plikiem...");

            }
        }
    }
    */


    /**
     * Pobranie pliku z serwera
     */
    private class DownloadFileTask extends AsyncTask<String, Void, Message> {

        String filenameDownload;
        @Override
        protected Message doInBackground(String... params) {
            try {
                final String url = "http://" + AuthData.serverAddress +"/download";

                filenameDownload = params[0];

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                Message pull_req_message = new Message(Message.TYPE_PULL_REQ,params[0]);

                Message response = restTemplate.postForObject(url,pull_req_message,Message.class);
                return response;

            } catch (Exception e) {
                Log.e("Download Task", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Message response) {


            if (response!=null) {

                if (response.getCommand().equals(Message.TYPE_FILE_TRANSFER_PULL) && response.verify(AuthData.privateKey)) {

                    progressDialog.setMessage("Obliczanie HMAC pliku...");

                    String pathDownloadedFile = getSharedPreferences("PREFERENCES", 0).getString("path", Environment.getExternalStorageDirectory().getAbsolutePath() + "/SafeCloudFiles/")+"/"+filenameDownload;
                    Log.d("MAMA MIA",pathDownloadedFile);


                    try {

                        File file = new File(pathDownloadedFile);
                        file.createNewFile();


                        byte[] fileBytes = Base64.decode(response.getArgument(), Base64.DEFAULT);

                        FileUtils.writeByteArrayToFile(file, fileBytes);

                    } catch (IOException e) {
                        e.printStackTrace();
                        AppViewUtils.displayDialog(FilesActivity.this,"Błąd",e.getLocalizedMessage());
                    } catch (Exception e){
                        e.printStackTrace();
                        AppViewUtils.displayDialog(FilesActivity.this,"Błąd",e.getLocalizedMessage());
                    }





                } else if(response.getCommand().equals(Message.TYPE_ERROR) && response.verify(AuthData.privateKey)) {
                    AppViewUtils.displayDialog(FilesActivity.this,"Blad",response.getArgument());
                } else {
                    AppViewUtils.displayDialog(FilesActivity.this,"Blad","Niespodziewana badź nieautoryzowana odpowiedź serwera");
                }
            } else {
                AppViewUtils.displayDialog(FilesActivity.this,"Blad","Brak odpowiedzi");
            }

            progressDialog.dismiss();

        }
    }


    /**
     * Aktualizacja tekstu okna dialogowego informujacego o trwajacym dzialaniu spoza watku UI
     * @param text_arg tekst
     */
    public void updateProgressMessage(String text_arg){

        final String text = text_arg;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDialog.setMessage(text);

            }
        });

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_files, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.files_action_upload) {
            Intent intent = new Intent(FilesActivity.this, com.orleonsoft.android.simplefilechooser.ui.FileChooserActivity.class);
            startActivityForResult(intent, FILE_CHOOSER);
            return true;
        }
        if (id == R.id.files_action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }
        if (id == R.id.files_action_logout) {
            // wylogowanie - zapewnienie prywatnosci na urzadzeniu
            // AuthData.privateKey = "null";
            AuthData.username="";
            MainActivity.editUser.setText("");
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
