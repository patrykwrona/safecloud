package pl.com.adapat.safecloud.crypto;

import android.util.Log;

import java.math.BigInteger;

import java.security.SecureRandom;


import pl.com.adapat.safecloud.comm.AuthData;

/**
 * Created by patryk on 06.06.15.
 */


//TODO: naprawic test

/**
 * Wymiana klucza algorytmem Diffiego-Hellmana
 */
public class DiffieHelmann {


    // klucze publiczne
    // g - generator grupy (ale chyba nie musi nim koniecznie byc?)
    // p - liczba modularna

    /**
     * klucz publiczny P
     */
    static BigInteger p = new BigInteger(AuthData.publicKeyP);


    /**
     * klucz publiczny G
     */
    static BigInteger g = new BigInteger(AuthData.publicKeyG);


    /**
     * losowana liczba
     */
    public static int a;


    /**
     * Wygenerowanie liczby ktora zostanie wyslana do drugiej strony:
     * A = G^a (mod P)
     *
     * @return liczba A
     */
    public static String genA() {

        BigInteger A;
        SecureRandom rand = new SecureRandom();

        int los = 0;
        while (los <= 256) {
            los = rand.nextInt(1000);
        }

        a = los;
        Log.d("DH", "wylosowane a:" + a);

        A = g;
        A = A.pow(a);
        Log.d("DH", "g^a:" + A);
        A = A.mod(p);
        Log.d("DH", "g^a (mod p):" + A);

        //A jest wartoscia ktora trzeba wyslac do drugiej strony
        return A.toString();
    }


    /**
     * Obliczenie klucza prywatnego
     *
     * @param B_string liczba otrzymana od drugiej strony
     * @return klucz tajny
     */
    public static String calculateSecret(String B_string) {

        BigInteger B = new BigInteger(B_string);
        BigInteger S;
        Log.d("DH", "otrzymane od drugiej strony B:" + B);

        Log.d("DH", "a wynosi:" + a);


        //obliczamy S: S= B^a (mod p)

        S = B.pow(a);
        Log.d("DH", "B^a:" + S);

        S = S.mod(p);
        Log.d("DH", "B^a (mod p) [klucz tajny]:" + S);

        //S jest obliczonym sekretem - kluczem tajnym

        return S.toString();
    }


    /**
     * [Do testow] (z zapamietywaniem wartosci losowanej, zeby zasymulowac obie strony)
     *
     * @param B_string
     * @param los
     * @return
     */
    public static String calculateSecretFrom_a(String B_string,int los,String g_arg,String p_arg) {

        BigInteger g = new BigInteger(g_arg);
        BigInteger p = new BigInteger(p_arg);

        BigInteger B = new BigInteger(B_string);
        BigInteger S;

        //obliczamy S: S= B^a (mod p)

        S = B.pow(los);
        S = S.mod(p);

        //S jest obliczonym sekretem - kluczem tajnym

        return S.toString();
    }


    /**
     * [Do testow] (uzywamy kluczy publicznych podanych przez uzytkownika)
     *
     * @param g_str
     * @param p_str
     * @return
     */
    public static String genAfromPandG(String g_str, String p_str) {

        BigInteger gt=new BigInteger(g_str);
        BigInteger pt=new BigInteger(p_str);

        BigInteger A;
        SecureRandom rand = new SecureRandom();

        int los = 0;
        while (los <= 256) {
            los = rand.nextInt(1000);
        }

        a = los;
        Log.d("DH", "wylosowane a:" + a);

        A = gt;
        A = A.pow(a);
        Log.d("DH", "g^a:" + A);
        A = A.mod(pt);
        Log.d("DH", "g^a (mod p):" + A);

        //A jest wartoscia ktora trzeba wyslac do drugiej strony
        return A.toString();
    }


    /**
     * [Do testow] pobranie wartosci wylosowanej liczby
     * @return a
     */
    public static int getRandom_a(){
        return a;
    }

}

