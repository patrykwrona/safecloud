package pl.com.adapat.safecloud;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import android.view.MenuItem;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;

import pl.com.adapat.safecloud.comm.AuthData;
import pl.com.adapat.safecloud.comm.Message;

/**
 * Glowna aktywnosc widoczna po uruchomieniu aplikacji<br>
 *
 */
public class MainActivity extends Activity {

    static EditText editUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //final EditText editUser = (EditText) findViewById(R.id.id_editUser);
        editUser = (EditText) findViewById(R.id.id_editUser);
        final EditText editServerAddress = (EditText) findViewById(R.id.id_editServerAddress);

        Button buttonRegisterActivity = (Button) findViewById(R.id.id_buttonRegisterActivity);
        Button buttonLogin = (Button) findViewById(R.id.id_buttonLogin);

        //wczytanie danych z shared prefs
        SharedPreferences preferences = this.getSharedPreferences("PREFERENCES", 0);
        editServerAddress.setText(preferences.getString("server","192.168.1.1:8080/mkoi"));
        AuthData.privateKey=preferences.getString("key","0");

        //utworzenie domyslnego folderu jesli nie istnieje
        String defaultDirPath = getSharedPreferences("PREFERENCES", 0).getString("path", Environment.getExternalStorageDirectory().getAbsolutePath() + "/SafeCloudFiles/");
        if (!new File(defaultDirPath).exists()){
            new File(defaultDirPath).mkdir();
        }

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AuthData.username = editUser.getText().toString();
                AuthData.serverAddress = editServerAddress.getText().toString();

                //zapamietanie wprowadzonego adresu serwera
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("PREFERENCES", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("server",editServerAddress.getText().toString());
                editor.commit();

                startActivity(new Intent(getApplicationContext(), FilesActivity.class));

            }
        });

        buttonRegisterActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AuthData.serverAddress = editServerAddress.getText().toString();

                //zapamietanie wprowadzonego adresu serwera
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("PREFERENCES", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("server",editServerAddress.getText().toString());
                editor.commit();


                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.main_action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_test) {
            startActivity(new Intent(getApplicationContext(), TestCryptoActivity.class));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }






}
