package pl.com.adapat.safecloud.comm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.com.adapat.safecloud.crypto.RfcHmac;

/**
 * Created by patryk on 12.05.15.
 */

/**
 * Podstawowa jednostka wymieniana miedzy klientem a serwerem <br>
 * Wiadomosci te sa parsowane do formatu JSON i przesylane za pomoca protokolu HTTP  <br>
 *
 *
 *     Lista typow wiadomosci:<br>
 *<br>
 * nazwa (command)   &nbsp&nbsp&nbsp&nbsp     kierunek    &nbsp&nbsp&nbsp&nbsp    argument        &nbsp&nbsp&nbsp&nbsp                  adnotacje<br>
 *<br>
 * REGISTER_REQ     &nbsp&nbsp&nbsp&nbsp          K->S     &nbsp&nbsp&nbsp&nbsp     liczba A (DH)        &nbsp&nbsp&nbsp&nbsp           Stringi zawierajace duze liczby<br>
 * REGISTER_ACK     &nbsp&nbsp&nbsp&nbsp          S->K     &nbsp&nbsp&nbsp&nbsp     liczba B (DH)       &nbsp&nbsp&nbsp&nbsp            w postaci dziesietnej<br>
 *<br>
 * GET_LIST     &nbsp&nbsp&nbsp&nbsp              K->S     &nbsp&nbsp&nbsp&nbsp       - <br>
 * LIST         &nbsp&nbsp&nbsp&nbsp              S->K     &nbsp&nbsp&nbsp&nbsp     lista plikow w jednym stringu &nbsp&nbsp&nbsp&nbsp  rozdzielone przecinkami<br>
 *<br>
 * PULL_REQ      &nbsp&nbsp&nbsp&nbsp             K->S    &nbsp&nbsp&nbsp&nbsp      nazwa pliku<br>
 * FILE_TRANSFER_PULL    &nbsp&nbsp&nbsp&nbsp     S->K    &nbsp&nbsp&nbsp&nbsp      PLIK (base64)<br>
 *<br>
 * FILE_TRANSFER_PUSH   &nbsp&nbsp&nbsp&nbsp      K->S     &nbsp&nbsp&nbsp&nbsp     PLIK (base64)<br>
 *<br>
 * DEL_REQ      &nbsp&nbsp&nbsp&nbsp              K->S   &nbsp&nbsp&nbsp&nbsp       nazwa pliku<br>
 * DEL_ACK         &nbsp&nbsp&nbsp&nbsp           S->K      &nbsp&nbsp&nbsp&nbsp       -<br>
 *<br>
 * ERROR     &nbsp&nbsp&nbsp&nbsp                 <-->     &nbsp&nbsp&nbsp&nbsp     przyczyna      &nbsp&nbsp&nbsp&nbsp                 wysylana w przypadkach bledu<br>
 *
 *  */
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    /*
    Struktura kazdej wiadomosci (oprocz tych przenoszacych pliki)
    Wiadomosci beda sparsowane do JSONa
    --------------
    | timestamp  | czas utworzenia wiadomosci
    --------------
    | username   | uzytkownik ktory utworzyl wiadomosc
    -------------- > nie wiem co tu dac w przypadku wiadomosci od serwera, ale raczej nazwe uzytkownika albo nic, albo adres/nazwe serwera
    | command    | typ komendy (wypisane nizej)
    --------------
    | argument   | argument - tylko jeden np. nazwa pliku, lista plikow, liczby A,B (DH)
    --------------
    | hmac       | HMAC wiadomosci wyliczony na podstawie:
    -------------- > konkatenacja wszystkich powyzszych pol wiadomosci
                   > klucz prywatny (nie haslo uzytkownika)
     */

    /**
     * znacznik nadawany wiadomosci w chwili jej tworzenia - czas w milisekundach od 1 stycznia 1970 roku
     */
    private String timestamp;

    /**
     * nazwa uzytkownika - sluzy do jego identyfikacji na serwerze
     */
    private String username;


    /**
     * typ wiadomosci
     */
    private String command;

    /**
     * dodatkowe dane przenoszone w wiadomosci, np: plik,nazwa pliku, przyczyna bledu
     */
    private String argument;

    /**
     * nazwa pliku - tylko w przypadku wiadomosci FILE_TRANSFER_PUSH oraz FILE_TRANSFER_PULL
     */
    private String filename;

    /**
     * HMAC obliczony z konkatenacji wszystkich poprzednich pol wiadomosci
     */
    private String hmac;


    /*
    Lista typow wiadomosci

    nazwa (command)          kierunek        argument                          adnotacje

    REGISTER_REQ              K->S          liczba A (DH)                   Stringi zawierajace duze liczby
    REGISTER_ACK              S->K          liczba B (DH)                   w postaci dziesietnej

    GET_LIST                  K->S            -
    LIST                      S->K          lista plikow w jednym stringu   rozdzielone przecinkami

    PULL_REQ                  K->S          nazwa pliku
    FILE_TRANSFER_PULL        S->K          PLIK (base64)

    FILE_TRANSFER_PUSH        K->S          PLIK (base64)

    DEL_REQ                   K->S          nazwa pliku
    DEL_ACK                   S->K             -

    ERROR                     <-->          przyczyna                       wysylana w przypadkach bledu

     */
    public static final String TYPE_REGISTER_REQ = "REGISTER_REQ";
    public static final String TYPE_REGISTER_ACK = "REGISTER_ACK";

    public static final String TYPE_GET_LIST = "GET_LIST";
    public static final String TYPE_LIST = "LIST";

    public static final String TYPE_PULL_REQ = "PULL_REQ";
    public static final String TYPE_FILE_TRANSFER_PULL = "FILE_TRANSFER_PULL";

    public static final String TYPE_FILE_TRANSFER_PUSH = "FILE_TRANSFER_PUSH";

    public static final String TYPE_DEL_REQ = "DEL_REQ";
    public static final String TYPE_DEL_ACK = "DEL_ACK";

    public static final String TYPE_ERROR = "ERROR";


    public Message(){

    }


    /**
     * Konstruktor dla wiadomosci w ktorych jest obliczany HMAC.
     *
     * @param type typ wiadomosci
     * @param argument dodatkowe dane
     */
    public Message(String type, String argument){

        long time = System.currentTimeMillis();
        this.timestamp = String.valueOf(time);

        this.username = AuthData.username;

        this.command = type;

        this.argument = argument;

        this.filename = "filename";

        String message_text = timestamp+username+command+argument;
        this.hmac = RfcHmac.hmac(AuthData.privateKey,message_text);



    }



    /**
     * Konstruktor dla wiadomosci  REGISTER (REQ oraz ACK) - nie jest obliczna dla nich HMAC poniewaz nie ma wtedy jeszcze ustalonego klucza tajnego.
     * Nalezy podac w nim nazwe uzytkownika, poniewaz nie jest jeszcze wtedy wpisana do AuthData
     * Jest on takze uzywany do utworzenia wiadomosci ERROR w przypadku gdy nie ma jeszcze ustalonego klucza,
     * (gdy REGISTER_REQ bedzie probowal tworzyc uzytkownika o nazwie ktory juz istnieje w bazie.
     *
     * @param type typ wiadomosci
     * @param argument dane, w tym przypadku liczba wysylana do drugiej strony w algorytmie DH
     * @param username nowo wybrana nazwa uzytkownika
     */
    public Message(String type, String argument, String username){

        long time = System.currentTimeMillis();
        this.timestamp = String.valueOf(time);

        this.username = username;

        this.command = type;

        this.argument = argument;

        this.filename = "filename";

        this.hmac = "0000";



    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }


    public String getFilename() { return filename; }


    public void setFilename(String filename) {
        this.filename = filename;
    }


    /**
     * Sprawdzamy czy obliczony HMAC zgadza sie z tym dostarczonym w wiadomosci.
     * Dodatkowo sprawdzamy czy wiadomosc zostala wygenerowana przed uplynieciem timeoutu ustawionego w AuthData.
     * Zapobiega to sytuacji w ktorej osoba trzecia moglaby przechwycic cala wiadomosc i wykorzystac poźniej.
     *
     * @param key klucz tajny
     * @return wynik weryfikacji
     */
    public boolean verify(String key){

        String message_text = timestamp+username+command+argument;
        String calculated_hmac = RfcHmac.hmac(key,message_text);

        // KONCEPT:
        // 0-OK, 1-zly HMAC, 2-za duzy timeout(ale HMAC dobry) [moze byc to proba ataku:
        // przechwycenie i wyslanie poprawnego pakietu poźniej]

        long timeout=AuthData.timeout*1000;
        if (calculated_hmac.equals(this.hmac) && (System.currentTimeMillis() - Long.valueOf(this.timestamp).longValue() < timeout)){
            return true;
        }

        return false;
    }


    @Override
    public String toString() {
        return "Message{" +
                "timestamp='" + timestamp + '\'' +
                ", username='" + username + '\'' +
                ", command='" + command + '\'' +
                ", argument='" + argument + '\'' +
                ", hmac='" + hmac + '\'' +
                ", filename='" + filename + '\'' +
                '}';
    }


}
