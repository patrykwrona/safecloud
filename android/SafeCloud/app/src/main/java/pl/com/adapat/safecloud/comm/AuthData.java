package pl.com.adapat.safecloud.comm;

/**
 * Created by patryk on 15.05.15.
 */


/**
 * Klasa zawierajaca zmienne kryptograficzne oraz inne parametry autoryzacyjne uzywane wewnatrz aplikacji
 *  */
public class AuthData {


    /**
     * nazwa uzytkownika - login wpisywany w aplikacji
     */
    public static String username;

    /**
     *  adres IP serwera wraz z portem
     */
    public static String serverAddress;


    /**
     * klucz prywatny do obliczania HMAC (ustalony przez algorytm DH)
     */
    public static String privateKey;

    /**
     *  klucz publiczny P (DH)
     */
    public static String publicKeyP = "1571";

    /**
     * klucz publiczny G (DH)
     */
    public static String publicKeyG = "5";


    /**
     * timeout w sekundach, uzywany do weryfikacji czy nie jest to waiadomosc przechwycona i ponownie wyslana
     */
    public static int timeout = 20;


}