package pl.com.adapat.safecloud;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.io.FileUtils;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import pl.com.adapat.safecloud.comm.AuthData;
import pl.com.adapat.safecloud.comm.Message;

// TODO: klasa debugownicza
// TODO: cała będzie do wywalenia


public class TestConnActivity extends Activity {

    TextView outputConnTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_conn);

        outputConnTest = (TextView) findViewById(R.id.id_textTestConnOutput);

        Button buttonConnTest1 = (Button) findViewById(R.id.id_buttonTestConn1);
        buttonConnTest1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // new TestConnTask().execute();

            }
        });
    }



    private class TestConnTask extends AsyncTask<Void, Void, Message> {


        @Override
        protected Message doInBackground(Void... params) {
            try {
              //  final String url = "http://" + AuthData.serverAddress + "/mkoi/upload";

                final String url = "http://192.168.0.11:8080";

                try {


                    File file = new File("/storage/emulated/0/SafeCloudFiles/index.php");
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                    Message push_file_message = new Message(Message.TYPE_FILE_TRANSFER_PUSH,
                            Base64.encodeToString(FileUtils.readFileToByteArray(file), Base64.DEFAULT));


                    restTemplate.postForObject(url, push_file_message, Message.class);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test_conn, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
