package pl.com.adapat.safecloud;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import pl.com.adapat.safecloud.R;
import pl.com.adapat.safecloud.comm.AuthData;
import pl.com.adapat.safecloud.comm.Message;
import pl.com.adapat.safecloud.crypto.DiffieHelmann;


/**
 * Aktywnosc dokonujaca rejestracji nowego uzytkownika w bazie, wraz z ustaleniem dla niego klucza tajnego za pomoca protokolu DH
 */
public class RegisterActivity extends Activity {

    TextView outputRegister;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText editNewUser = (EditText) findViewById(R.id.id_editNewUser);

        outputRegister = (TextView) findViewById(R.id.id_textRegisterOutput);

        Button buttonRegister = (Button) findViewById(R.id.id_buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newUser = editNewUser.getText().toString();
                progressDialog = ProgressDialog.show(RegisterActivity.this, "", "Rejestracja...");
                new RegisterTask().execute(newUser);

            }
        });

    }


    private class RegisterTask extends AsyncTask<String, Void, Message> {
        @Override
        protected Message doInBackground(String... params) {


            final String url = "http://" + AuthData.serverAddress +"/register";

            //Obliczamy liczbe A dla DH
            String A = DiffieHelmann.genA();

            //Tworzymy wiadomosc do wyslania
            Message register_req_message = new Message(Message.TYPE_REGISTER_REQ,A,params[0]);

            // Tworzymy instancje RestTemplate
            // Dodajemy konwerter wiadomosci do JSON
            RestTemplate restTemplate = new RestTemplate();
         //   restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            try {

                Message response = restTemplate.postForObject(url, register_req_message, Message.class);
                Log.d("CHUJ REGISTER",register_req_message.toString());
                return response;

            } catch (Exception ex){

                Log.e("Register Activity Conn", ex.getLocalizedMessage(), ex);
            }


            return null;
        }


        @Override
        protected void onPostExecute(Message response) {

            // sprawdzamy czy przyszla jakakolwiek odpowiedź
            if(response!=null) {

                //sprawdzamy czy typ odpowiedzi REGISTER_ACK
                if (response.getCommand().equals(Message.TYPE_REGISTER_ACK)) {

                    //odczytujemy z odpowiedzi wartosc B i generujemy na jej podstawie klucz prywatny
                    String B = response.getArgument();
                    AuthData.privateKey = DiffieHelmann.calculateSecret(B);

                    // zapisujemy go do shared prefs
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("PREFERENCES", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("key",AuthData.privateKey);
                    editor.commit();


                    outputRegister.setText("Konto utworzone!" + "\n" + "Cofnij, aby sie zalogowac"+"\n\n");
                    outputRegister.append("Nazwa uzytkownika: " + response.getUsername() + "\n" + "Ustalony klucz tajny: " + AuthData.privateKey);

                } else if (response.getCommand().equals(Message.TYPE_ERROR)) {
                    outputRegister.setText("Blad: " + response.getArgument());
                } else {
                    outputRegister.setText("Nieoczekiwana odpowiedź serwera");
                }

            } else {
                outputRegister.setText("Brak odpowiedzi");
            }
            
            progressDialog.dismiss();
        }
    }



}

