package pl.com.adapat.safecloud;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pl.com.adapat.safecloud.comm.AuthData;
import pl.com.adapat.safecloud.comm.Message;
import pl.com.adapat.safecloud.crypto.DiffieHelmann;
import pl.com.adapat.safecloud.crypto.Keccak;
import pl.com.adapat.safecloud.crypto.RfcHmac;


/**
 * Aktywnosc umozliwiajaca wykonanie testow modulu kryptograficznego<br>
 *     Przyciski umozliwiaja wykonanie testow:<br>
 *         obliczanie SHA-3<br>
 *         obliczanie HMAC<br>
 *         test protokolu Diffiego-Hellmana<br>
 *         test weryfikacji wiadomosci<br>
 */
public class TestCryptoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);


        final TextView outputTest = (TextView) findViewById(R.id.id_testHmac);
        final EditText testKey = (EditText) findViewById(R.id.id_editTestKey);
        final EditText testMessage = (EditText) findViewById(R.id.id_editTestMessage);
        Button buttonTestHmac = (Button) findViewById(R.id.id_buttonTestHmac);
        buttonTestHmac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key= testKey.getText().toString();
                String message= testMessage.getText().toString();
                String hmaczek = RfcHmac.hmac(key, message);
                outputTest.setText(hmaczek);
                Log.d("TEST ACTIVITY - HMAC",hmaczek);
            }
        });


        Button buttonTestSha = (Button) findViewById(R.id.id_buttonTestSha);
        buttonTestSha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message= testMessage.getText().toString();
                Keccak kecc = new Keccak(1600);
                String shaczek = kecc.getHash(RfcHmac.hexify(message.getBytes()),1088,32);
                outputTest.setText(shaczek);
                Log.d("TEST ACTIVITY - SHA3",shaczek);
            }
        });

        final EditText testG = (EditText) findViewById(R.id.id_editG);
        final EditText testP = (EditText) findViewById(R.id.id_editP);
        Button buttonTestDH = (Button) findViewById(R.id.id_buttonTestDH);
        buttonTestDH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                outputTest.setText("");
                String G_string= testG.getText().toString();
                String P_string = testP.getText().toString();

                String A = DiffieHelmann.genAfromPandG(G_string,P_string);
                int a_test = DiffieHelmann.getRandom_a();
                outputTest.append("K - wylosowal liczbe a: "+a_test+"\n");
                outputTest.append("K - liczba wyslana do S - G^a (mod P): "+A+"\n");

                String B = DiffieHelmann.genAfromPandG(G_string,P_string);
                int b_test = DiffieHelmann.getRandom_a();
                outputTest.append("S - wylosowal liczbe b: "+b_test+"\n");
                outputTest.append("S - liczba wyslana do K - G^b (mod P): "+B+"\n");

                String sA = DiffieHelmann.calculateSecretFrom_a(B,a_test,G_string,P_string);
                String sB = DiffieHelmann.calculateSecretFrom_a(A,b_test,G_string,P_string);
                outputTest.append("K - obliczyl tajny klucz: "+sA+"\n");
                outputTest.append("S - obliczyl tajny klucz: "+sB+"\n");
            }
        });


        Button buttonTestVerify = (Button) findViewById(R.id.id_buttonVerify);
        buttonTestVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //wygenerowanie i sprawdzenie
                outputTest.append("privateKey: "+AuthData.privateKey+"\n");

                Message m = new Message(Message.TYPE_GET_LIST,"");
                outputTest.append("Wygenerowana wiadomosc GET_LIST"+"\n");
                outputTest.append("verify: "+m.verify(AuthData.privateKey)+"\n");
                outputTest.append("zmieniono timestamp+1 \n");
                m.setTimestamp(m.getTimestamp()+1);
                outputTest.append("verify: "+m.verify(AuthData.privateKey));

            }
        });

    }


}
