package pl.com.adapat.safecloud;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import pl.com.adapat.safecloud.comm.AuthData;

/**
 * Zmiana ustawien - katalogu przechowywania plikow na urzadzeniu
 */
public class SettingsActivity extends Activity {

    final int FOLDER_CHOOSER = 2;

    TextView textCurrentDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Button buttonSave = (Button) findViewById(R.id.id_button_Save);
        Button buttonSelectDir = (Button) findViewById(R.id.id_buttonSelectDir);
        textCurrentDir = (TextView) findViewById(R.id.id_textCurrentDir);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PREFERENCES", 0);
        textCurrentDir.setText(preferences.getString("path", Environment.getExternalStorageDirectory().getAbsolutePath()+"/SafeCloudFiles"));


        buttonSelectDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(SettingsActivity.this, com.orleonsoft.android.simplefilechooser.ui.FolderChooserActivity.class);
                startActivityForResult(intent, FOLDER_CHOOSER);


            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = getApplicationContext().getSharedPreferences("PREFERENCES", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("path",textCurrentDir.getText().toString());
                editor.commit();
                finish();


            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == FOLDER_CHOOSER) && (resultCode == RESULT_OK)) {
            String fileSelected = data.getStringExtra(com.orleonsoft.android.simplefilechooser.Constants.KEY_FILE_SELECTED);
            Log.d("FILE SELECTED", fileSelected);
            textCurrentDir.setText(fileSelected);
        }
    }

}