package pl.com.adapat.safecloud.crypto;

import android.util.Log;

/**
 * Created by patryk on 11.05.15.
 */


/**
 * Klasa obliczajaca HMAC wg specyfikacji RFC 2104
 */
public class RfcHmac {


    /**
     * Parametr oznaczony jako B w RFC
     * okresla on dlugosc paddingow ipad oraz opad, a tym samym dlugosc wykorzystanego klucza w bajtach
     * gdy klucz jest dluzszy zostanie obciety do pierwszych B bajtow.
     */
    static int B_len = 64;


    /**
     * Konwersja tablicy bajtow na String, przedstawiajacy ich wartosci w hexach
     *
     * @param buf bajty do konwersji
     * @return przedstawienie wartosci hex
     */
    public static String hexify (byte [] buf)
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++)
        {
            Byte b= new Byte(buf[i]);
            String s = Integer.toHexString(b.intValue());
            if (s.length() == 1)
                s = "0" + s;
            if (s.length()>2)
                s= s.substring(s.length()-2);
            sb.append(s);
        }
        return sb.toString();
    }


    /**
     * Konwersja Stringa z wartosciami w hexach na tablice bajtow
     *
     * @param s przedstawienie wartosci hex
     * @return tablica bajtow
     */
    public static byte[] unhexify(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


    /**
     * Obliczanie wartosci HMAC
     *
     * @param secret klucz tajny
     * @param message wiadomosc do zaszyfrowania
     * @return string przedstawiajacy wartosc HMAC w hex
     */
    public static String hmac(String secret, String message){


        byte [] K = secret.getBytes();

        byte [] m = message.getBytes();
        byte [] ipad = new byte[B_len];
        byte [] opad = new byte[B_len];


        // inicjalizacja ipad oraz opad odpowiednimi bajtami
        // oraz przy okazji xoruje ipad oraz opad z kluczem
        for ( int i = 0; i < B_len; i++) {
            if ( i < secret.length() ) {
                byte b = (byte)secret.charAt(i);
                ipad[i] = (byte)( b ^ 0x36);
                opad[i] = (byte)( b ^ 0x5c);
            }
            else {
                ipad[i] = 0x36;
                opad[i] = 0x5c;
            }

        }

        // konkatenacja ipad(xor)K | m
        // symbol '|' oznacza konkatenacje
        byte[] inner = new byte[ipad.length + m.length];
        System.arraycopy(ipad, 0, inner, 0, ipad.length);
        System.arraycopy(m, 0, inner, ipad.length, m.length);
        Log.d("RFC HMAC","ipad.length="+ ipad.length);
        Log.d("RFC HMAC","m.length="+ m.length);
        Log.d("RFC HMAC","inner.length="+ inner.length);
        Log.d("RFC HMAC","inner[]="+hexify(inner));

        // obliczenie SHA-3 dla  ipad(xor)K | m
        // argument konstruktora to dlugosc stanu/rejestru w algorytmnie
        // dla standardu SHA-3 musi to byc 1600
        Keccak kec = new Keccak(1600);
        String innerHashStr = kec.getHash(hexify(inner),1088,32);
        Log.d("RFC HMAC","innerHash.length="+ innerHashStr.getBytes().length);
        Log.d("RFC HMAC","innerHash[]="+hexify(innerHashStr.getBytes()));

        byte[] innerHash = innerHashStr.getBytes();

        // konkatenacja opad(xor)X | h( ipad(xor)K | m )
        byte[] outer = new byte[opad.length + innerHashStr.length()];
        System.arraycopy(opad, 0, outer, 0, opad.length);
        System.arraycopy(innerHash, 0, outer, opad.length, innerHash.length);
        Log.d("RFC HMAC","opad.length="+ opad.length);
        Log.d("RFC HMAC","outer[]="+hexify(outer));

        // SHA-3 dla calosci - gotowe HMAC
        String outerHashStr = kec.getHash(hexify(outer),1088,32);
        Log.d("RFC HMAC","outerHash.length="+ outerHashStr.length());


        return outerHashStr;
    }

}
